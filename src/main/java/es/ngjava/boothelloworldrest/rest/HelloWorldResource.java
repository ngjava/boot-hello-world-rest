package es.ngjava.boothelloworldrest.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class HelloWorldResource {

	@GetMapping("")
	public ResponseEntity<String> helloWorld(){
		return ResponseEntity.ok().body("Hello World!");
	}
	
}
