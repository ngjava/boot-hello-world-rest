package es.ngjava.boothelloworldrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootHelloWorldRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootHelloWorldRestApplication.class, args);
	}
}
